NeuroDebian Policy
---

1. Preamble and User Promise

   1.1 [NeuroDebian GNU/Linux](http://neuro.debian.net) (or "NeuroDebian") is derivative project of Debian GNU/Linux (or "Debian") for researchers focused on neuroimaging, data management and related studies.

   1.2 NeuroDebian repository hosts:

      - software releases compliant with [Debian Free Software Guidelines (DFSG)](http://www.debian.org/social_contract#guidelines) in its 'main' section,
      - software releases available under restrictive licenses in 'contrib' and 'non-free' sections,
      - neuroimaging datasets available under permissive licenses in the 'datasets' section.

   1.3 Software releases available in NeuroDebian repository are distrbuted as source and binary packages for the following Linux distributions:

      - Debian GNU/Linux,
      - Ubuntu Linux,

      as well as Virtual Machine appliances ready to be deployed in any operating system supported by Oracle VirtualBox.

   1.4 New versions of packages available in NeuroDebian repository SHOULD be built for the following suites of supported Linux distributions:

      - Debian GNU/Linux: unstable, testing, and still supported by Debian project or community supported Extended Long-Term Support (ELTS) releases (listed at https://wiki.debian.org/DebianReleases), 
      - Ubuntu Linux: devel, current testing release, releases including Long-Term Support (LTS) releases (listed at https://ubuntu.com/about/release-cycle),

      for the following architectures:

      - amd64,
      - arm64,
      - i386,

     where supported by the base distribution.

   The support cycles of packages available in NeuroDebian repository are synchronized with respective upstream's ones on the "best effort" basis.

   1.5 The security updates for packages available in NeuroDebian are prepared in accordance with [Debian Security Team guidelines](https://wiki.debian.org/Teams/Security).

2. Packaging Guidelines

   2.1 Packages available in NeuroDebian repository MUST conform to [Debian Policy Manual](https://www.debian.org/doc/debian-policy/index.html)

     2.1.1 Source package MUST be buildable against the respective Debian and NeuroDebian repository without errors.

     2.1.2 Resulting binary packages MUST pass all the tests available in the source package, with exemptions on individual basis.

     2.1.3 Packages entering unstable release of NeuroDebian archive ("neurodebian/sid") MUST pass lintian(1) checks without errors.

     2.1.4 Resulting binary packages MUST pass automatic package tests ("autopkgtests") if such tests are available for the source package.

   2.2 Packages available in NeuroDebian repository MUST depend only on binary packages available in respective Debian and NeuroDebian release archives.

     2.2.1 Packages available in NeuroDebian repository SHOULD NOT require major upgrades of components like (but not limited to):

      - Kernel and key Debian packages (glibc, musl etc),
      - Compilers and interpreters of programming languages (GCC, Clang, Python, Go, Rust etc).

    2.2.2 Debian helper tools requirement CAN be downgraded for releases earlier than "sid".

3. Packaging Repository Guidelines

   3.1 Source trees for packages available in NeuroDebian repository SHOULD be stored in Git repositories on [Debian Salsa](https://salsa.debian.org) GitLab instance.

      3.1.1 Git repositories for packages primarily maintained by NeuroDebian team members SHOULD be stored in [NeuroDebian Team package subspace](https://salsa.debian.org/neurodebian-team/packages):

      3.1.2 Git repositories for packages co-maintained by NeuroDebian team members CAN be stored in other parts of Debian Salsa, if:

      - they were introduced to Debian or NeuroDebian archive prior to the adoption of ths Policy,
      - primary Debian maintainers have no objections against keeping Git branches related to NeuroDebian in the repository.

      3.1.3 Git repositories for packages not maintained by NeuroDebian team members SHOULD be mirrored into NeuroDebian Team package subspace, if:

      - primary Debian maintainers object against keeping Git branches related to NeuroDebian in the repository,
      - primary Debian maintainers prefer version control system (VCS) different from Git or use no VCS at all.

   3.2 Git repositories SHOULD be organized in accordance with [Debian's DEP-14 standard](https://dep-team.pages.debian.net/deps/dep14/):

      - Git branches holding packaging for Debian official archive (http://deb.debian.org) for a given release codename SHOULD be named "debian/<codename>",
      - Git branches holding packaging for NeuroDebian official archive for a given release codename SHOULD be named "neurodebian/<codename>",
      - Git tags for NeuroDebian packages SHOULD be named "neurodebian/<mangled-debian-version>",
      - "debian/control" should have "Vcs-" header fields adjusted to point to NeuroDebian's GitLab repository.

   3.3 Vendor-specific patches and configuration files to build NeuroDebian package from Debian package MUST be stored in "debian/patches/neurodebian" sub-directory.

---
